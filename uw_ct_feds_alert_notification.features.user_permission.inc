<?php

/**
 * @file
 * uw_ct_feds_alert_notification.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_feds_alert_notification_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create feds_alert_notification content'.
  $permissions['create feds_alert_notification content'] = array(
    'name' => 'create feds_alert_notification content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any feds_alert_notification content'.
  $permissions['delete any feds_alert_notification content'] = array(
    'name' => 'delete any feds_alert_notification content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own feds_alert_notification content'.
  $permissions['delete own feds_alert_notification content'] = array(
    'name' => 'delete own feds_alert_notification content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any feds_alert_notification content'.
  $permissions['edit any feds_alert_notification content'] = array(
    'name' => 'edit any feds_alert_notification content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own feds_alert_notification content'.
  $permissions['edit own feds_alert_notification content'] = array(
    'name' => 'edit own feds_alert_notification content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter feds_alert_notification revision log entry'.
  $permissions['enter feds_alert_notification revision log entry'] = array(
    'name' => 'enter feds_alert_notification revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_alert_notification authored by option'.
  $permissions['override feds_alert_notification authored by option'] = array(
    'name' => 'override feds_alert_notification authored by option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_alert_notification authored on option'.
  $permissions['override feds_alert_notification authored on option'] = array(
    'name' => 'override feds_alert_notification authored on option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_alert_notification comment setting option'.
  $permissions['override feds_alert_notification comment setting option'] = array(
    'name' => 'override feds_alert_notification comment setting option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_alert_notification promote to front page option'.
  $permissions['override feds_alert_notification promote to front page option'] = array(
    'name' => 'override feds_alert_notification promote to front page option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_alert_notification published option'.
  $permissions['override feds_alert_notification published option'] = array(
    'name' => 'override feds_alert_notification published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_alert_notification revision option'.
  $permissions['override feds_alert_notification revision option'] = array(
    'name' => 'override feds_alert_notification revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_alert_notification sticky option'.
  $permissions['override feds_alert_notification sticky option'] = array(
    'name' => 'override feds_alert_notification sticky option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'search feds_alert_notification content'.
  $permissions['search feds_alert_notification content'] = array(
    'name' => 'search feds_alert_notification content',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'search_config',
  );

  return $permissions;
}
