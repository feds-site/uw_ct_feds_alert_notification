<?php

/**
 * @file
 * uw_ct_feds_alert_notification.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_feds_alert_notification_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'feds_alert_notification';
  $context->description = '';
  $context->tag = 'Feds';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '*' => '*',
        '<front>' => '<front>',
      ),
    ),
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-fbfd0eb7a6fdf26d47850f347fec0f49' => array(
          'module' => 'views',
          'delta' => 'fbfd0eb7a6fdf26d47850f347fec0f49',
          'region' => 'alert_notification',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Feds');
  $export['feds_alert_notification'] = $context;

  return $export;
}
