<?php

/**
 * @file
 * uw_ct_feds_alert_notification.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_ct_feds_alert_notification_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: navigation_feds-alert--notification:node/add/feds-alert-notification.
  $menu_links['navigation_feds-alert--notification:node/add/feds-alert-notification'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/feds-alert-notification',
    'router_path' => 'node/add/feds-alert-notification',
    'link_title' => 'Feds Alert & Notification',
    'options' => array(
      'attributes' => array(
        'title' => 'Text only alert or notification. Should only be used for highly important and timely notices. ',
      ),
      'identifier' => 'navigation_feds-alert--notification:node/add/feds-alert-notification',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
    'parent_identifier' => 'navigation_add-content:node/add',
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Feds Alert & Notification');

  return $menu_links;
}
