<?php

/**
 * @file
 * uw_ct_feds_alert_notification.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function uw_ct_feds_alert_notification_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'feds_alert_and_notifications';
  $view->description = 'Displayed at the top of the site for urgent alerts and important notifications';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Feds Alert and Notifications';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Feds Alert and Notifications';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_type'] = 'div';
  $handler->display->display_options['fields']['title']['element_class'] = 'alert-main-title';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Display Title */
  $handler->display->display_options['fields']['field_display_title']['id'] = 'field_display_title';
  $handler->display->display_options['fields']['field_display_title']['table'] = 'field_data_field_display_title';
  $handler->display->display_options['fields']['field_display_title']['field'] = 'field_display_title';
  $handler->display->display_options['fields']['field_display_title']['label'] = '';
  $handler->display->display_options['fields']['field_display_title']['element_type'] = 'div';
  $handler->display->display_options['fields']['field_display_title']['element_class'] = 'alert-display-title';
  $handler->display->display_options['fields']['field_display_title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_display_title']['element_default_classes'] = FALSE;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['element_type'] = 'div';
  $handler->display->display_options['fields']['body']['element_class'] = 'alert-body';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['element_default_classes'] = FALSE;
  /* Field: Content: Type */
  $handler->display->display_options['fields']['field_type']['id'] = 'field_type';
  $handler->display->display_options['fields']['field_type']['table'] = 'field_data_field_type';
  $handler->display->display_options['fields']['field_type']['field'] = 'field_type';
  $handler->display->display_options['fields']['field_type']['element_type'] = '0';
  $handler->display->display_options['fields']['field_type']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_type']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_type']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_type']['type'] = 'list_key';
  /* Field: Content: Bell Icon */
  $handler->display->display_options['fields']['field_bell_icon']['id'] = 'field_bell_icon';
  $handler->display->display_options['fields']['field_bell_icon']['table'] = 'field_data_field_bell_icon';
  $handler->display->display_options['fields']['field_bell_icon']['field'] = 'field_bell_icon';
  $handler->display->display_options['fields']['field_bell_icon']['element_type'] = 'div';
  $handler->display->display_options['fields']['field_bell_icon']['element_class'] = 'alert-icon';
  $handler->display->display_options['fields']['field_bell_icon']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_bell_icon']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_bell_icon']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_bell_icon']['empty_zero'] = TRUE;
  $handler->display->display_options['fields']['field_bell_icon']['type'] = 'list_key';
  /* Field: Content: Colour Scheme */
  $handler->display->display_options['fields']['field_colour_scheme']['id'] = 'field_colour_scheme';
  $handler->display->display_options['fields']['field_colour_scheme']['table'] = 'field_data_field_colour_scheme';
  $handler->display->display_options['fields']['field_colour_scheme']['field'] = 'field_colour_scheme';
  $handler->display->display_options['fields']['field_colour_scheme']['element_type'] = '0';
  $handler->display->display_options['fields']['field_colour_scheme']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_colour_scheme']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_colour_scheme']['type'] = 'list_key';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published status */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'feds_alert_notification' => 'feds_alert_notification',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;

  /* Display: Feds Alert and Notification Block */
  $handler = $view->new_display('block', 'Feds Alert and Notification Block', 'feds_alert_notification');
  $handler->display->display_options['block_description'] = 'Feds Alert Notification Block';
  $translatables['feds_alert_and_notifications'] = array(
    t('Master'),
    t('Feds Alert and Notifications'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Type'),
    t('Bell Icon'),
    t('Colour Scheme'),
    t('Feds Alert and Notification Block'),
    t('Feds Alert Notification Block'),
  );
  $export['feds_alert_and_notifications'] = $view;

  return $export;
}
