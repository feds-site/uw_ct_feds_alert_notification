<?php

/**
 * @file
 * uw_ct_feds_alert_notification.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_ct_feds_alert_notification_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_notification_options|node|feds_alert_notification|form';
  $field_group->group_name = 'group_notification_options';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feds_alert_notification';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Notification Options',
    'weight' => '4',
    'children' => array(
      0 => 'field_bell_icon',
      1 => 'field_colour_scheme',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-notification-options field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_notification_options|node|feds_alert_notification|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Notification Options');

  return $field_groups;
}
